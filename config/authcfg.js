module.exports.authcfg = 
{
  inviteTknAlgorithm: "sha256",
  inviteTknLen: 64,
  inviteTknExpiresIn: 86400,
  saltFactor: 10
};