var bcrypt = require("bcrypt");
var SALT_WORK_FACTOR = sails.config.authcfg.saltFactor;

module.exports = {

  /**
  * Hash the password field of the passed user. Asynchronous)
  */
  hashPassword: function(password, cb) {
  	console.log("Salt Factor", SALT_WORK_FACTOR);
    return bcrypt.hash(password, SALT_WORK_FACTOR, cb);
  }
};