module.exports =
{
	randomgen: function(len)
	{
		var text = "";
    var possible = "abcdefghijklmnopqrstuzwxyz0123456789";

    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
	}
};