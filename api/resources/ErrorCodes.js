/* FileName: ErrorCodes.js
* @description: This file contains all the error codes and respective messages.
*
* Date                        Change Description                               Author
* ---------               ---------------------------                        -----------
* 25/12/2016                Initial file creation                             Smruti Mandal
*
* Error Codes:
*
* Types of standard error codes:
*
* 1. bad Request (400)
* 2. Forbidden (403)
* 3. not found (404)
* 4. Server Error (500)
*
* each type will be further customized as following:
*
* For custom bad request errors it will be 400XXX. Where each module will have its own reservation of 50 codes, except for system related errors which have 100 reserved for them. Hence
*
* 1 – 50    System
* 51 - 100  Misc

The above holds true for all standard error codes

*/

module.exports =
{
  // System Error Codes
  500001:"System Error. Please contact your administrator",

  //Authentication Error Codes - 501 - 550
  401501: "Invalid token supplied",

  //Authorization Error Codes
  403501:"The requester is not an admin of the BD",
  403502:"The requester doesn't have the required privilege",

  // Misc Error Codes
  400051:"Incorrect Parameters. Correct and try again",
  400052:"Calculated accountid is a duplicate",
  400053:"Broker is taking too long to come-up. Giving up",
  400054:"No free slot for accounts",
  400055:"Incorrect captcha",

  // Attachment Error Codes - 401 - 410
  400401:"No file attached",
  400402:"Requested file doesn't exist",
  400403:"Maximum supported file size is 5MB",
  400405:"One or more of the attachments do not exist",
  400406:"Please provide unique set of attachments",
};
