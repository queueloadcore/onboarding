/* FileName: Account.js
 *
 * @description: This file describes the attributes of an account
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 05/12/2016        Initial file creation                                       SMandal
 *
 */

 module.exports =
{
  tableName: "accounts",
  
  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  
  attributes:
  {
    id:
    {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    accountid:
    {
      type: 'string',
      maxLength: 8,
      minLength: 8,
      unique: true,
      required: true
    },
    priceplan:
    {
      type: 'string',
      maxLength: 32,
      required: true
    },
    name:
    {
      type: 'string',
      maxLength: 128,
      required: true
    },
    address:
    {
      type: 'string',
      maxLength: 512,
      required: true,
    },
    url:
    {
      type: 'string',
      url: true,
      required: true
    },
    database:
    {
      type: 'string',
      maxLength: 64,
      required: true
    },
    s3bucket:
    {
      type: 'string',
      maxLength: 1024
    },
    custadmin:
    {
      type: 'string',
      email: true,
      required: true
    },
    acctmgr:
    {
      type: 'string',
      email: true
    },
    isactive:
    {
      type: 'boolean',
      required: true,
      defaultsTo: true
    },
    config:
    {
      type: 'json'
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};