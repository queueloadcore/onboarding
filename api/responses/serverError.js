/**
 * 500 (Server Error) Response
 *
 * A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
 *
 * Usage:
 * return res.serverError();
 * return res.serverError(err);
 * return res.serverError(err, 'some/specific/error/view');
 *
 * NOTE:
 * If something throws in a policy or controller, or an internal
 * error is encountered, Sails will call `res.serverError()`
 * automatically.
 */

 var _ = require("lodash");

module.exports = function serverError(data, config)
{

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(500);

  var response = _.assign(
    {
      code: _.get(config, "code", "E_SERVER_ERROR"),
      message: _.get(config, "message", "The request cannot be fulfilled due to bad syntax"),
      data: data || {}
    }, _.get(config, "root", {}));

    var details = JSON.stringify(data, null, 4);

  // Log error to console
  sails.log.error('Sending 500 ("Server Error") response: \n', response.code, "\n", response.message, "\n", details, "\n", config.stack);

  // Only include errors in response if application environment
  // is not set to 'production'.  In production, we shouldn't
  // send back any identifying information about errors.
  //TBD: Need to confirm this
  if (sails.config.environment === "production")
  {
    var code = response.code;
    response = {};
    response.code = code;
  }

  return res.jsonx(response);

};
