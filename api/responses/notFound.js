/**
 * 404 (Not Found) Handler
 *
 * The requested resource could not be found but may be available again in the future.
 * Subsequent requests by the client are permissible.
 *
 * Usage:
 * return res.notFound();
 * return res.notFound(err);
 * return res.notFound(err, 'some/specific/notfound/view');
 *
 * e.g.:
 * ```
 * return res.notFound();
 * ```
 *
 * NOTE:
 * If a request doesn't match any explicit routes (i.e. `config/routes.js`)
 * or route blueprints (i.e. "shadow routes", Sails will call `res.notFound()`
 * automatically.
 */

 var _ = require("lodash");

module.exports = function notFound(data, config)
{
  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(404);

  var response = _.assign(
    {
      code: _.get(config, "code", "E_NOT_FOUND"),
      message: _.get(config, "message", "The request cannot be fulfilled due to bad syntax"),
      data: data || {}
    }, _.get(config, "root", {}));

  // Log error to console
  sails.log.verbose('Sending 404 ("Not Found") response: \n', response);

  // Only include errors in response if application environment
  // is not set to 'production'.  In production, we shouldn't
  // send back any identifying information about errors.
  //TBD: Need to confirm this
  if (sails.config.environment === "production")
  {
    var code = response.code;
    response = {};
    response.code = code;
  }

  return res.jsonx(response);
};
