# Introduction


[QueueLoad](https://bitbucket.org/29bones/queueload-0.1) is an attempt to reduce alert fatigue and help organizations run silent operations.

Onboarding is a nodejs application server which accepts requests through a RESTful interface and creates a Queueload account for the customer.

The application can be run as a standalone. However we recommend Docker. You can use the attached Dockerfile to build the image and run it.

# Requirements 

1. AWS Account
2. SendGrid Account
3. Slack (Optional)
4. Docker-CE, Swarm (Optional)

# Questions and issues

Feel free to use the issues tracker for any questions and issues. I may be a bit slow on response and I hope you understand that.

# License
The MIT License (MIT)

Copyright (c) 2016-2018 QueueLoad

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
